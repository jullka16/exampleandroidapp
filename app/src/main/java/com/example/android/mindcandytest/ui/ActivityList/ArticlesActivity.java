package com.example.android.mindcandytest.ui.ActivityList;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.android.mindcandytest.MyApplication;
import com.example.android.mindcandytest.R;
import com.example.android.mindcandytest.model.Content;
import com.example.android.mindcandytest.ui.ActivityComments.CommentsActivity;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Retrofit;

public class ArticlesActivity extends AppCompatActivity implements ArticlesActivityContract.View {
    public static final String ARTICLE_KEY = "article_key";

    @BindView(R.id.articles_recycler_view)
    RecyclerView articlesRecyclerView;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    private List<Content> articles = new ArrayList<>();
    private ArticlesAdapter adapter;
    private ArticlesActivityContract.Presenter presenter;

    @Inject
    Retrofit retrofit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_articles);
        ButterKnife.bind(this);

        ((MyApplication) getApplication()).getAppComponent().inject(this);

        setUpAdapter();

        presenter = new ArticlesActivityPresenterImpl(this, retrofit);
        presenter.fetchAndDisplayData();
    }

    private void setUpAdapter() {
        adapter = new ArticlesAdapter(articles, this, new ArticlesAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String premalink) {
                openBrowserForArticle(premalink);
            }

            @Override
            public void onButtonClick(String articleId) {
                showCommentsActivity(articleId);
            }
        });
        articlesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        articlesRecyclerView.setAdapter(adapter);
    }

    private void openBrowserForArticle(String premalink) {
        String url = MyApplication.BASE_URL + premalink;
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        startActivity(intent);
    }


    private void showCommentsActivity(String articleId) {
        Intent intent  = new Intent(this, CommentsActivity.class);
        intent.putExtra(ARTICLE_KEY, articleId);
        startActivity(intent);
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.dispose();
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.dispose();
    }

    @Override
    public void setArticleList(List<Content> articles) {
        adapter.updateArticlesList(articles);
    }

    @Override
    public void hideProgressSpinner() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showErrorMessage() {
        Toast.makeText(this, getResources().getString(R.string.error_message), Toast.LENGTH_SHORT).show();
    }
}
