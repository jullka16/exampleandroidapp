package com.example.android.mindcandytest.ui.ActivityList;

import com.example.android.mindcandytest.model.Content;

import java.util.List;

public interface ArticlesActivityContract {
    interface View{
        void setArticleList(List<Content> articles);
        void hideProgressSpinner();
        void showErrorMessage();
    }

    interface Presenter {
        void fetchAndDisplayData();
        void dispose();
    }

}
