package com.example.android.mindcandytest.ui.ActivityComments;

import java.util.List;

public interface CommentsActivityContract {
    interface View{
        void setCommentsList(List<String> comments);
        void hideProgressSpinner();
        void showErrorMessage();
        void showNoCommentsMessage();
    }

    interface Presenter {
        void fetchAndDisplayData();
        void dispose();
    }
}
