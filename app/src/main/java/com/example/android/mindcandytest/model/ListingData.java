package com.example.android.mindcandytest.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ListingData {
    @SerializedName("dist")
    String dist;

    @SerializedName("children")
    List<Child> children;

    public ListingData(String dist, List<Child> children) {
        this.dist = dist;
        this.children = children;
    }

    public String getDist() {
        return dist;
    }

    public List<Child> getChildren() {
        return children;
    }
}
