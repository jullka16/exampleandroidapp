package com.example.android.mindcandytest;

import android.app.Application;

import com.example.android.mindcandytest.di.AppComponent;
import com.example.android.mindcandytest.di.DaggerAppComponent;
import com.example.android.mindcandytest.di.NetworkModule;

public class MyApplication extends Application {
    private AppComponent appComponent;
    public static final String BASE_URL = "https://www.reddit.com/";

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = DaggerAppComponent.builder()
                .networkModule(new NetworkModule(BASE_URL))
                .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

}
