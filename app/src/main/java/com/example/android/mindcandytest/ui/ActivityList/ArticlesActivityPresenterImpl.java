package com.example.android.mindcandytest.ui.ActivityList;

import com.example.android.mindcandytest.model.Content;
import com.example.android.mindcandytest.model.Child;
import com.example.android.mindcandytest.model.Listing;
import com.example.android.mindcandytest.networking.Endpoints;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class ArticlesActivityPresenterImpl implements ArticlesActivityContract.Presenter {
    private ArticlesActivityContract.View view;
    private CompositeDisposable disposables;
    private Retrofit retrofit;

    public ArticlesActivityPresenterImpl(ArticlesActivityContract.View view, Retrofit retrofit) {
        this.view = view;
        this.retrofit = retrofit;
        disposables = new CompositeDisposable();
    }

    @Override
    public void fetchAndDisplayData() {
        disposables.add(retrofit.create(Endpoints.class).getListing()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<Listing>(){
                    @Override
                    public void onNext(Listing listing) {
                        view.hideProgressSpinner();
                        List<Content> articles = new ArrayList<>();
                        List<Child> children = listing.getListingData().getChildren();
                        for(Child child : children){
                            articles.add(child.getContent());
                        }
                        view.setArticleList(articles);
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.hideProgressSpinner();
                        view.showErrorMessage();
                    }

                    @Override
                    public void onComplete() {

                    }
                }));
    }

    @Override
    public void dispose() {
        disposables.clear();
        disposables.dispose();
    }
}
