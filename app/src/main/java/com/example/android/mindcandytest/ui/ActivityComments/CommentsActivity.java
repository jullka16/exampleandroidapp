package com.example.android.mindcandytest.ui.ActivityComments;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.mindcandytest.MyApplication;
import com.example.android.mindcandytest.R;
import com.example.android.mindcandytest.ui.ActivityList.ArticlesActivity;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Retrofit;

public class CommentsActivity extends AppCompatActivity implements CommentsActivityContract.View {
    @BindView(R.id.articles_recycler_view)
    RecyclerView articlesRecyclerView;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.no_comments)
    TextView noComments;

    private List<String> comments = new ArrayList<>();
    private CommentsActivityContract.Presenter presenter;
    private CommentsAdapter adapter;

    @Inject
    Retrofit retrofit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);

        ButterKnife.bind(this);
        ((MyApplication) getApplication()).getAppComponent().inject(this);
        setUpAdapter();

        String articleId = "";
        if(getIntent().getExtras() != null){
            articleId = getIntent().getExtras().getString(ArticlesActivity.ARTICLE_KEY);
        }
        presenter = new CommentsActivityPresenterImpl(this, retrofit, articleId);
        presenter.fetchAndDisplayData();
    }

    private void setUpAdapter() {
        adapter = new CommentsAdapter(comments);
        articlesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        articlesRecyclerView.setAdapter(adapter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.dispose();
    }

    @Override
    public void setCommentsList(List<String> comments) {
        adapter.updateCommentsList(comments);
    }

    @Override
    public void hideProgressSpinner() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showErrorMessage() {
        Toast.makeText(this, getResources().getString(R.string.error_message), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showNoCommentsMessage() {
        articlesRecyclerView.setVisibility(View.GONE);
        noComments.setVisibility(View.VISIBLE);
    }
}
