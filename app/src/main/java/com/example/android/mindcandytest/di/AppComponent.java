package com.example.android.mindcandytest.di;

import com.example.android.mindcandytest.ui.ActivityComments.CommentsActivity;
import com.example.android.mindcandytest.ui.ActivityList.ArticlesActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = NetworkModule.class)
public interface AppComponent {
    void inject(ArticlesActivity articlesActivity);
    void inject(CommentsActivity commentsActivity);
}
