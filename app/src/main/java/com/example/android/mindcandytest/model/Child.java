package com.example.android.mindcandytest.model;

import com.google.gson.annotations.SerializedName;

public class Child {
    @SerializedName("kind")
    String kind;

    @SerializedName("data")
    Content content;

    public Child(String kind, Content content) {
        this.kind = kind;
        this.content = content;
    }

    public String getKind() {
        return kind;
    }

    public Content getContent() {
        return content;
    }
}
