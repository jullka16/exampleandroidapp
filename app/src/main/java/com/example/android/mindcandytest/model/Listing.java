package com.example.android.mindcandytest.model;

import com.google.gson.annotations.SerializedName;

public class Listing {
    @SerializedName("kind")
    String kind;

    @SerializedName("data")
    ListingData listingData;

    public Listing(String kind, ListingData listingData) {
        this.kind = kind;
        this.listingData = listingData;
    }

    public String getKind() {
        return kind;
    }

    public ListingData getListingData() {
        return listingData;
    }
}
