package com.example.android.mindcandytest.ui.ActivityComments;

import com.example.android.mindcandytest.model.Child;
import com.example.android.mindcandytest.model.Listing;
import com.example.android.mindcandytest.networking.Endpoints;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class CommentsActivityPresenterImpl implements CommentsActivityContract.Presenter {
    private static final String TAG = CommentsActivityPresenterImpl.class.getSimpleName();

    private CommentsActivityContract.View view;
    private CompositeDisposable disposables;
    private Retrofit retrofit;
    private String articleId;

    public CommentsActivityPresenterImpl(CommentsActivityContract.View view, Retrofit retrofit, String articleId) {
        this.view = view;
        this.retrofit = retrofit;
        this.articleId = articleId;
        disposables = new CompositeDisposable();
    }

    @Override
    public void fetchAndDisplayData() {
        disposables.add(retrofit.create(Endpoints.class).getArticleComments(articleId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<List<Listing>>(){

                    @Override
                    public void onNext(List<Listing> listings) {
                        view.hideProgressSpinner();
                        List<Child> children = listings.get(1).getListingData().getChildren();
                        if (children.size() == 0) {
                            view.showNoCommentsMessage();
                        } else {
                            List<String> comments = new ArrayList<>();
                            for (Child child : children) {
                                comments.add(child.getContent().getBody());
                            }
                            view.setCommentsList(comments);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.hideProgressSpinner();
                        view.showErrorMessage();
                    }

                    @Override
                    public void onComplete() {
                    }
                }));

    }

    @Override
    public void dispose() {
        disposables.clear();
        disposables.dispose();
    }
}
