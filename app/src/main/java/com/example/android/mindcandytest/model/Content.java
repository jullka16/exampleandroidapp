package com.example.android.mindcandytest.model;

import com.google.gson.annotations.SerializedName;

public class Content {
    @SerializedName("selftext")
    String selfText;

    @SerializedName("title")
    String title;

    @SerializedName("id")
    String id;

    @SerializedName("body")
    String body;

    @SerializedName("permalink")
    String permalink;


    public Content(String selfText, String title, String id, String body, String permalink) {
        this.selfText = selfText;
        this.title = title;
        this.id = id;
        this.body = body;
        this.permalink = permalink;
    }

    public String getSelfText() {
        return selfText;
    }

    public String getTitle() {
        return title;
    }

    public String getId() {
        return id;
    }

    public String getBody() {
        return body;
    }

    public String getPermalink() {
        return permalink;
    }
}
