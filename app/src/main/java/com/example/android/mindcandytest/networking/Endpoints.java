package com.example.android.mindcandytest.networking;

import com.example.android.mindcandytest.model.Listing;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface Endpoints {

    @GET("new.json")
    Observable<Listing> getListing();

    @GET("comments/{id}.json")
    Observable<List<Listing>> getArticleComments(@Path("id") String id);
}
