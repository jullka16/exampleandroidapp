package com.example.android.mindcandytest.ui.ActivityList;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.android.mindcandytest.R;
import com.example.android.mindcandytest.model.Content;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ArticlesAdapter extends RecyclerView.Adapter<ArticlesAdapter.MyViewHolder> {
    private List<Content> articles;
    private Context context;
    private OnItemClickListener myListener;

    public interface OnItemClickListener {
        void onItemClick(String articleId);
        void onButtonClick(String premalink);
    }

    public ArticlesAdapter(List<Content> articles, Context context, OnItemClickListener myListener) {
        this.articles = articles;
        this.context = context;
        this.myListener = myListener;
    }

    public void updateArticlesList(List<Content> articles) {
        this.articles = articles;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.article_item, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        Content article = articles.get(i);
        myViewHolder.articleTitle.setText(article.getTitle());
        myViewHolder.articleContent.setText(article.getSelfText());
        myViewHolder.articleRow.setOnClickListener((view) -> myListener.onItemClick(article.getPermalink()));
        myViewHolder.commentsButton.setOnClickListener((view) -> myListener.onButtonClick(article.getId()));
    }

    @Override
    public int getItemCount() {
        return articles.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.article_title)
        TextView articleTitle;
        @BindView(R.id.article_content)
        TextView articleContent;
        @BindView(R.id.comments_button)
        Button commentsButton;
        @BindView(R.id.article_row)
        LinearLayout articleRow;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
